use hashbrown::hash_map::HashMap;
use internship::IStr;
use petgraph::visit::{Bfs, Reversed};
use petgraph::graph::NodeIndex;
use nom::{
    IResult,
    Err,
    branch::alt,
    combinator::{map, map_res},
    sequence::{delimited},
    bytes::complete::{tag, take_while, is_not},
    character::complete::{digit1, multispace0}
};
use std::convert::TryFrom;

use crate::TupleIx;
use crate::context::{
    Context, Metadata,
    builder::ContextBuilder,
    value::ValId
};

const WHITESPACE_AND_SIGILS : &str = " \t\r\n(){}[]:#";

/// Parser associated value metadata
#[derive(Debug, Clone)]
pub struct ParserMetadata {
    pub name : IStr
}

impl Metadata for ParserMetadata {
    /// Get metadata, if any, associated with the unit type
    #[inline] fn get_unit_meta() -> Option<Self> { Some(Self{ name : "#unit".into() }) }
    /// Get metadata, if any, associated with the empty type
    #[inline] fn get_empty_meta() -> Option<Self> { Some(Self{ name : "#empty".into() }) }
    /// Get metadata, if any, associated with the finite universe
    #[inline] fn get_universe_meta() -> Option<Self> { Some(Self{ name : "#finite".into() }) }
    /// Get the name, if any, of the given object
    #[inline] fn get_name(&self) -> Option<&str> { Some(&self.name) }
    /// Get the interned name, if any, of the given object
    #[inline] fn get_interned_name(&self) -> Option<IStr> { Some(self.name.clone()) }
}

/// An owned parsing context
#[derive(Debug)]
pub struct  ParsingContext<'a, M> {
    builder : ContextBuilder<'a, M>,
    name_map : HashMap<IStr, Vec<ValId>>
}

impl<'a, M: Metadata> ParsingContext<'a, M> {
    /// Create a new parsing context, borrowing a given context.
    /// Parse from the scopes of the values given, starting with the first in the given list
    pub fn with_scope(
        builder: ContextBuilder<'a, M>,
        scopes: &[ValId]) -> Self {

        let context = builder.ctx();
        let mut bfs = Bfs::new(Reversed(context.value_graph()), NodeIndex::new(0));
        let mut name_map = HashMap::new();

        bfs.stack.clear();
        for scope in scopes.iter() { bfs.stack.push_front(scope.ix()) }

        while let Some(nx) = bfs.next(context.value_graph()) {
            let v = ValId::assert_valid(nx);
            if let Some(name) = context.get_meta(v)
                .map(|m| m.get_interned_name())
                .unwrap_or(None) {
                name_map.entry(name).or_insert(vec![v]);
            }
        }

        ParsingContext { builder, name_map }
    }
    /// Create a new parsing context, borrowing a given context.
    /// Parse from the root.
    pub fn new(builder: ContextBuilder<'a, M>) -> Self {
        let root = builder.ctx().get_root();
        Self::with_scope(builder, &[root])
    }
    /// Get a reference to the underlying context of this value context
    pub fn ctx(&self) -> &Context<M> { self.builder.ctx() }
    /// Get a mutable reference to the builder of this parsing context
    pub fn builder(&mut self) -> &mut ContextBuilder<'a, M> { &mut self.builder }
}

/// A token
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Token<'a> {
    /// Left parenthesis
    LPar,
    /// Right parenthesis
    RPar,
    /// Left curly brace
    LCr,
    /// Right curly brace
    RCr,
    /// Left bracket
    LBr,
    /// Right bracket
    RBr,
    /// Scope delimiter ("::")
    Scp,
    /// Colon
    Col,
    /// Hash
    Hash,
    /// A valid name for a variable, like "x12"
    Symbol(&'a str),
    /// A valid number, like "123"
    Number(u128), // TODO: big integers
    /// A valid string, like ""Hello World!""
    String(&'a str)
}

/// Parse a symbol
pub fn parse_symbol(input: &str) -> IResult<&str, &str> {
    is_not(WHITESPACE_AND_SIGILS)(input)
}

/// Parse a number
//TODO: hex, et al
pub fn parse_number(input: &str) -> IResult<&str, u128> {
    map_res(digit1, |digit: &str| digit.parse::<u128>())(input)
}

/// Parse a string
pub fn parse_string(input: &str) -> IResult<&str, &str> {
    delimited(tag("\""), take_while(|c| c != '\"'), tag("\""))(input)
}

/// Parse a token
pub fn parse_token(input: &str) -> IResult<&str, Token> {
    use Token::*;
    alt((
        map(tag("("), |_| LPar),
        map(tag(")"), |_| RPar),
        map(tag("{"), |_| LCr),
        map(tag("}"), |_| RCr),
        map(tag("["), |_| LBr),
        map(tag("]"), |_| RBr),
        map(tag("::"), |_| Scp),
        map(tag(":"), |_| Col),
        map(tag("#"), |_| Hash),
        map(parse_number, |num| Number(num)),
        map(parse_symbol, |sym| Symbol(sym)),
        map(parse_string, |s| String(s))
    ))(input)
}

impl<'a, M: Metadata + From<ParserMetadata>> ParsingContext<'a, M> {
    /// Try to parse a string into a value. Return a value parsed in the given context.
    pub fn parse_value<'b>(&mut self, input: &'b str) -> IResult<&'b str, ValId> {
        let (rest, tok) = delimited(multispace0, parse_token, multispace0)(input)?;
        use Token::*;
        match tok {
            LPar => {
                // Parse sexpr
                let (rest, vals) = self.parse_values(rest)?;
                let (rest, tok) = parse_token(rest)?;
                // Unclosed sexpr
                if tok != RPar {
                    Err(Err::Failure((input, nom::error::ErrorKind::Tag)))
                } else { // Syntactically valid sexpr
                    Ok((rest, self.builder.build_sexpr(vals.iter().cloned()).weaken()))
                }
            },
            LCr => {
                // Parse qexpr
                let (rest, vals) = self.parse_values(rest)?;
                let (rest, tok) = parse_token(rest)?;
                // Unclosed qexpr
                if tok != RCr {
                    Err(Err::Failure((input, nom::error::ErrorKind::Tag)))
                } else { // Syntactically valid qexpr
                    Ok((rest, self.builder.build_tuple(vals.iter().cloned()).weaken()))
                }
            },
            LBr => {
                // Parse projection
                let (rest, proj) = map_res(
                    delimited(multispace0, parse_number, multispace0),
                    |num| TupleIx::try_from(num))(rest)?;
                let (rest, val) = self.parse_value(rest)?;
                let (rest, tok) = parse_token(rest)?;
                if tok != RBr {
                    Err(Err::Failure((input, nom::error::ErrorKind::Tag)))
                } else {
                    Ok((
                        rest,
                        self.builder.build_proj(val, proj, Default::default()).def().weaken())
                    )
                }
            },
            Hash => {
                // Parse special
                unimplemented!()
            },
            Number(_n) => {
                // Number value
                unimplemented!()
            },
            Symbol(_s) => {
                // Variable
                unimplemented!()
            },
            String(_s) => {
                // String value
                unimplemented!()
            },
            // Dirty hack...
            _ => Err(Err::Failure((input, nom::error::ErrorKind::Alt)))
        }
    }
    /// Try to parse a list of (space delimited) values into a vector
    pub fn parse_values<'b>(&mut self, mut input: &'b str) -> IResult<&'b str, Vec<ValId>> {
        let mut dest = Vec::new();
        loop {
            let (rest, val) = match self.parse_value(input) {
                Ok(v) => v,
                Err(e) => match e {
                    Err::Incomplete(_) => { return Err(e); },
                    _ => { return Ok((input, dest)); }
                }
            };
            dest.push(val);
            input = rest;
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::context::{
        Context, InferredType,
        builder::ContextBuilder,
        value::{ValId, DefId, TypeId}
    };
    use super::{ParsingContext, ParserMetadata};
    #[test]
    fn empty_sexpr_parsed_as_nil() {
        let mut ctx : Context<ParserMetadata> = Context::empty_with_capacity(0, 0, 0);
        let mut pc = ParsingContext::new(ContextBuilder::new(&mut ctx));
        let (rest, nil) = pc.parse_value("()").unwrap();
        assert_eq!(rest, "");
        assert!(pc.ctx().is_nil(nil));
        let unit = match pc.builder().infer_type(nil).expect("Unit type") {
            InferredType::HasType(t) => t,
            _ => panic!("Did not infer type properly")
        };
        assert!(pc.ctx().is_unit(unit));
    }
    #[test]
    fn empty_qexpr_parsed_as_nil() {
        let mut ctx : Context<ParserMetadata> = Context::empty_with_capacity(0, 0, 0);
        let mut pc = ParsingContext::new(ContextBuilder::new(&mut ctx));
        let (rest, nil) = pc.parse_value("{}").unwrap();
        assert_eq!(rest, "");
        assert!(pc.ctx().is_nil(nil));
        let unit = match pc.builder().infer_type(nil).expect("Unit type") {
            InferredType::HasType(t) => t,
            _ => panic!("Did not infer type properly")
        };
        assert!(pc.ctx().is_unit(unit));
    }
    #[test]
    fn nested_nil_parses() {
        let mut ctx : Context<ParserMetadata> = Context::empty_with_capacity(0, 0, 0);
        let mut pc = ParsingContext::new(ContextBuilder::new(&mut ctx));
        let (rest, nested_nil) = pc.parse_value("(())").unwrap();
        assert_eq!(rest, "");
        //TODO: type assertions, et al.
        let unit = match pc.builder().infer_type(nested_nil).expect("Unit type") {
            InferredType::HasType(t) => t,
            _ => panic!("Did not infer type properly")
        };
        assert!(pc.ctx().is_unit(unit));
    }
    #[test]
    fn multi_nil_parses() {
        let mut ctx: Context<ParserMetadata> = Context::empty_with_capacity(0, 0, 0);
        let mut pc = ParsingContext::new(ContextBuilder::new(&mut ctx));
        let (rest, nils) = pc.parse_value("{{} () {} ()}").unwrap();
        assert_eq!(rest, "");

        let mut arity: usize = 0;
        let mems: Vec<ValId> = pc.ctx().quote_members(DefId::assert_valid(nils.ix())).collect();
        for mem in mems {
            assert!(pc.ctx().is_nil(mem));
            let unit = match pc.builder().infer_type(mem).expect("Unit type") {
                InferredType::HasType(t) => t,
                _ => panic!("Did not infer type properly")
            };
            assert!(pc.ctx().is_unit(unit));
            arity += 1;
        }
        assert_eq!(arity, 4);

        let product = match pc.builder().infer_type(nils).expect("Unit type") {
            InferredType::HasType(t) => t,
            _ => panic!("Did not infer type properly")
        };
        assert!(!pc.ctx().is_unit(product));
        let mut ty_arity: usize = 0;
        let tys: Vec<TypeId> = pc.ctx()
            .product_members(DefId::assert_valid(product.ix()))
            .collect();
        for ty in tys {
            assert!(pc.ctx().is_unit(ty));
            ty_arity += 1;
        }

        assert_eq!(ty_arity, 4)
    }
    #[test]
    fn multi_nil_proj_parses() {
        let mut ctx : Context<ParserMetadata> = Context::empty_with_capacity(0, 0, 0);
        let mut pc = ParsingContext::new(ContextBuilder::new(&mut ctx));
        let (rest, _) = pc.parse_value("[1 {{} () {} ()}]").unwrap();
        assert_eq!(rest, "");
    }
}
