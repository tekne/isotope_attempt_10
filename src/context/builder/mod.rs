use petgraph::graph::NodeIndex;
use petgraph::Direction;
use petgraph::visit::EdgeRef;

use crate::{TupleIx, ValueIx};
use super::{
    Context,
    InferredType, TypeError,
    value::{
        DefId, TypeId, ValId, Definition, DefEdge,
        kind::{Kind, Product, Sum, Function, Expr, Quote, Cat, Lambda, DefinitionKind}
    },
    edge::{ValueEdge, EdgeKind}
};

pub mod state;
use state::{NewDef, BuiltState, BuiltDef};
pub mod inference;

/// A builder for a context
#[derive(Debug)]
pub struct ContextBuilder<'a, M> {
    ctx: &'a mut Context<M>,
    cached_nil: DefId<Expr>,
    cached_unit: TypeId,
    cached_empty: TypeId
}

impl<'a, M> ContextBuilder<'a, M> {
    /// Create a new context builder borrowing the given context
    #[inline]
    pub fn new(ctx : &'a mut Context<M>) -> ContextBuilder<'a, M> {
        ContextBuilder{
            cached_nil : ctx.get_new_nil(),
            cached_unit : ctx.get_new_unit(),
            cached_empty : ctx.get_new_empty(),
            ctx
        }
    }
    /// Get the nil value of this context builder
    #[inline(always)] pub fn get_nil(&self) -> DefId<Expr> { self.cached_nil }
    /// Get the unit value of this context builder
    #[inline(always)] pub fn get_unit(&self) -> TypeId { self.cached_unit }
    /// Get the empty value of this context builder
    #[inline(always)] pub fn get_empty(&self) -> TypeId { self.cached_empty }
    /// Get a reference to the underlying context of this context builder
    #[inline(always)] pub fn ctx(&self) -> &Context<M> { &self.ctx }
    /// Get a mutable reference to the underlying context of this context builder
    //TODO: consider making this private...
    #[inline(always)] pub fn ctx_mut(&mut self) -> &mut Context<M> { &mut self.ctx }

    /// Make a tuple of a set of values
    pub fn build_tuple<V>(&mut self, mut vals: V) -> ValId where V: Iterator<Item=ValId> {
        let result : ValId = if let Some(first) = vals.next() {
            let (_, node) = self.ctx.value_dag
                .add_child(first.ix(), EdgeKind::Expr{ input : 0 }, Definition::Quote(None));
            ValId::assert_valid(node)
        } else { return self.get_nil().weaken() };
        for (pos, val) in vals.enumerate() {
            self.ctx.value_dag.add_edge(
                val.ix(), result.ix(), EdgeKind::Expr{ input : pos as TupleIx + 1 })
                .expect("Unexpected cycle detected!");
        }
        result
    }
    /// Make a sexpr of a set of values
    //TODO: type checking
    pub fn build_sexpr<V>(&mut self, mut vals: V) -> DefId<Expr> where V: Iterator<Item=ValId> {
        let result = if let Some(function) = vals.next() {
            let (_, node) = self.ctx.value_dag
                .add_child(function.ix(), EdgeKind::Expr{ input : 0 }, Definition::Expr(None));
            DefId::assert_valid(node)
        } else { return self.get_nil() };
        for (pos, val) in vals.enumerate() {
            self.ctx.value_dag.add_edge(
                val.ix(), result.ix(), EdgeKind::Expr{ input : pos as TupleIx + 1 })
                .expect("Unexpected cycle detected!");
        }
        result
    }
    /// Make a projection
    pub fn build_new_proj(&mut self, val: ValId, proj: TupleIx) -> NewDef<Expr> {
        //TODO: checking?
        let (_, node) = self.ctx.value_dag.add_child(
            val.ix(), EdgeKind::Proj{ input : 0, proj }, Definition::Expr(None)
        );
        NewDef::assert_valid(DefId::assert_valid(node))
    }
    /// Make a projection, normalizing the tuple case.
    pub fn build_proj(&mut self, val: ValId, proj: TupleIx, target: BuiltState) -> BuiltDef<Expr> {
        //TODO: normalize
        self.build_new_proj(val, proj).weaken()
    }
    /// Make the product type of a set of types
    pub fn build_product<T>(&mut self, mut tys: T) -> TypeId where T: Iterator<Item=TypeId> {
        let result = if let Some(first) = tys.next() {
            let (_, node) = self.ctx.value_dag
                .add_child(first.ix(), EdgeKind::Expr{ input : 0 }, Definition::Product(None));
            TypeId::assert_valid(node)
        } else { return self.get_unit() };
        for (pos, ty) in tys.enumerate() {
            self.ctx.value_dag.add_edge(
                ty.ix(), result.ix(), EdgeKind::Expr{ input : pos as TupleIx + 1 })
                .expect("Unexpected cycle detected");
        }
        result
    }
    /// Make the sum type of a set of types
    pub fn build_sum<T>(&mut self, mut tys: T) -> TypeId where T: Iterator<Item=TypeId> {
        let result = if let Some(first) = tys.next() {
            let (_, node) = self.ctx.value_dag
                .add_child(first.ix(), EdgeKind::Expr{ input : 0 }, Definition::Sum(None));
            TypeId::assert_valid(node)
        } else { return self.get_empty() };
        for (pos, ty) in tys.enumerate() {
            self.ctx.value_dag.add_edge(
                ty.ix(), result.ix(), EdgeKind::Expr{ input : pos as TupleIx + 1 })
                .expect("Unexpected cycle detected");
        }
        result
    }
    /// Make a function type from one type to another
    pub fn build_simple_func_type(&mut self, domain: TypeId, target: TypeId) -> TypeId {
        let (_, res_node) = self.ctx.value_dag.add_child(
            domain.ix(), EdgeKind::expr(0), Definition::Function(None)
        );
        self.ctx.value_dag.add_edge(target.ix(), res_node, EdgeKind::const_proj(1, 0))
            .expect("Unexpected cycle detected");
        TypeId::assert_valid(res_node)
    }
}

#[cfg(test)]
mod tests {
    use super::{Context, ContextBuilder, InferredType};
    #[test]
    fn nil_is_nil() {
        let mut ctx: Context<()> = Context::empty_with_capacity(0, 0, 0);
        let mut builder = ContextBuilder::new(&mut ctx);
        assert!(builder.ctx().is_nil(builder.get_nil().weaken()));
        assert_eq!(
            builder.infer_type(builder.get_nil().weaken()).expect("Nil is a well-typed value"),
            InferredType::HasType(builder.get_unit())
        );
    }
    #[test]
    fn unit_is_unit() {
        let mut ctx: Context<()> = Context::empty_with_capacity(0, 0, 0);
        let builder = ContextBuilder::new(&mut ctx);
        assert!(builder.ctx().is_unit(builder.get_unit()))
    }
}
