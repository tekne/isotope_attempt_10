use super::*;

impl<'a, M> ContextBuilder<'a, M> {
    /// Set a reinferred non-universe type, overriding any old type
    fn set_reinferred_ty(&mut self, val: ValId, ty: TypeId) -> Result<InferredType, TypeError> {
        self.ctx.value_dag
            .add_edge(ty.ix(), val.ix(), EdgeKind::Type)
            .map_err(|_| TypeError::CyclicTypeDep)?;
        Ok(InferredType::HasType(ty))
    }
    /// Set a reinferred type, overriding any old type
    fn set_reinferred(&mut self, val: ValId, ty: InferredType) -> Result<InferredType, TypeError> {
        match ty {
            InferredType::HasType(ty) => self.set_reinferred_ty(val, ty),
            _ => Ok(ty)
        }
    }
    /// Infer the type of a product, overriding the old type if necessary
    pub fn reinfer_product_type(&mut self, val: DefId<Product>)
    -> Result<InferredType, TypeError> {
        unimplemented!()
    }
    /// Infer the type of a sum, overriding the old type if necessary
    pub fn reinfer_sum_type(&mut self, val: DefId<Sum>) -> Result<InferredType, TypeError> {
        unimplemented!()
    }
    /// Infer the type of a function type, overriding the old type if necessary
    pub fn reinfer_function_type(&mut self, val: DefId<Function>)
    -> Result<InferredType, TypeError> {
        unimplemented!()
    }
    /// Infer the type of an expression, overriding the old type if necessary
    pub fn reinfer_expr_type(&mut self, val: DefId<Expr>) -> Result<InferredType, TypeError> {
        let arguments: Vec<ValId> = self.ctx.value_graph()
            .edges_directed(val.ix(), Direction::Incoming)
            .filter(|edge| edge.weight().is_value())
            .map(|edge| ValId::assert_valid(edge.source()))
            .collect();
        if arguments.len() == 0 {
            return self.set_reinferred_ty(val.weaken(), self.get_unit());
        }
        let argument_types: Result<Vec<InferredType>, TypeError> = arguments.into_iter()
            .map(|val| self.infer_type(val))
            .collect();
        let argument_types = argument_types?;
        if argument_types.len() == 1 {
            return self.set_reinferred(val.weaken(), argument_types[0]);
        }
        unimplemented!()
    }
    /// Infer the type of a quote, overriding the old type if necessary
    pub fn reinfer_quote_type(&mut self, val: DefId<Quote>) -> Result<InferredType, TypeError> {
        use InferredType::*;
        let members: Vec<ValId> = self.ctx.value_graph()
            .edges_directed(val.ix(), Direction::Incoming)
            .filter(|edge| edge.weight().is_value())
            .map(|edge| ValId::assert_valid(edge.source()))
            .collect();
        let member_types: Result<Vec<TypeId>, TypeError> = members.into_iter()
            .map(|val| self.infer_type(val))
            //TODO: this
            .map(|typ| match typ {
                Ok(HasType(t)) => Ok(t), Err(e) => Err(e), _ => unimplemented!()
            })
            .collect();
        let result_ty = self.build_product(member_types?.iter().cloned());
        self.set_reinferred_ty(val.weaken(), result_ty)
    }
    /// Infer the type of a catenation, overriding the old type if necessary
    pub fn reinfer_cat_type(&mut self, val: DefId<Cat>) -> Result<InferredType, TypeError> {
        unimplemented!()
    }
    /// Infer the type of a lambda function, overriding the old type if necessary
    pub fn reinfer_lambda_type(&mut self, val: DefId<Lambda>) -> Result<InferredType, TypeError> {
        unimplemented!()
    }
    /// Infer the type of a value, overriding the old type if necessary
    pub fn reinfer_type(&mut self, val: ValId) -> Result<InferredType, TypeError> {
        use DefinitionKind::*;
        use TypeError::*;
        match self.ctx.to_value_kind(val).kind() {
            Universe => Err(NotEnoughInfo),
            Product => self.reinfer_product_type(DefId::assert_valid(val.ix())),
            Sum => self.reinfer_sum_type(DefId::assert_valid(val.ix())),
            Function => self.reinfer_function_type(DefId::assert_valid(val.ix())),
            Expr => self.reinfer_expr_type(DefId::assert_valid(val.ix())),
            Quote => self.reinfer_quote_type(DefId::assert_valid(val.ix())),
            Cat => self.reinfer_cat_type(DefId::assert_valid(val.ix())),
            Lambda => self.reinfer_lambda_type(DefId::assert_valid(val.ix())),
            Symbol => Err(NotEnoughInfo)
        }
    }
    /// Infer the type of a value, or an error if the value is invalid
    #[inline]
    pub fn infer_type(&mut self, val: ValId) -> Result<InferredType, TypeError> {
        self.ctx.get_type(val).or_else(|err| match err {
            TypeError::NotEnoughInfo => self.reinfer_type(val),
            e => Err(e)
        })
    }
}
