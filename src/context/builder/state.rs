use std::convert::{TryFrom, TryInto};
use std::default::Default;
use crate::NonZeroTupleIx;

use super::*;

/// The strongest known state for a built value
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum BuiltState {
    /// A value which other values depend on. Shouldn't be mutated unless you own it!
    Used,
    /// A value which no other values depend on, but someone else might have a (valid) handle to
    Free,
    /// A value which no other values depend on, *and* nobody has yet been issued a valid handle to
    New
}

impl Default for BuiltState { #[inline(always)] fn default() -> Self { BuiltState::Used }  }

/// The strongest known state for a free value
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum FreeState {
    /// A value which no other values depend on, but someone else might have a (valid) handle to
    Free,
    /// A value which no other values depend on, *and* nobody has yet been issued a valid handle to
    New
}

impl Default for FreeState { #[inline(always)] fn default() -> Self { FreeState::Free }  }
impl From<FreeState> for BuiltState {
    fn from(f: FreeState) -> BuiltState {
        match f {
            FreeState::Free => BuiltState::Free,
            FreeState::New => BuiltState::New
        }
    }
}
impl TryFrom<BuiltState> for FreeState {
    type Error = Used;
    fn try_from(f: BuiltState) -> Result<FreeState, Used> {
        match f {
            BuiltState::Free => Ok(FreeState::Free),
            BuiltState::New => Ok(FreeState::New),
            _ => Err(Used)
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct Used;
impl From<Used> for BuiltState { fn from(_: Used) -> Self { BuiltState::Used } }
impl From<BuiltState> for Used { fn from(_: BuiltState) -> Self { Used } }
impl From<FreeState> for Used { fn from(_: FreeState) -> Self { Used } }
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct Free;
impl From<Free> for BuiltState { fn from(_: Free) -> Self { BuiltState::Free } }
impl From<Free> for FreeState { fn from(_: Free) -> Self { FreeState::Free } }
impl From<Free> for Used { fn from(_: Free) -> Self { Used } }
impl TryFrom<BuiltState> for Free {
    type Error = Used;
    fn try_from(b: BuiltState) -> Result<Free, Used> {
        match b {
            BuiltState::Free | BuiltState::New => Ok(Free),
            _ => Err(Used)
        }
    }
}
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct New;
impl From<New> for BuiltState { fn from(_: New) -> Self { BuiltState::New } }
impl From<New> for FreeState { fn from(_: New) -> Self { FreeState::New } }
impl From<New> for Free { fn from(_: New) -> Self { Free } }
impl From<New> for Used { fn from(_: New) -> Self { Used } }
impl TryFrom<BuiltState> for New {
    type Error = BuiltState;
    fn try_from(b: BuiltState) -> Result<New, BuiltState> {
        match b {
            BuiltState::New => Ok(New),
            b => Err(b)
        }
    }
}
impl TryFrom<FreeState> for New {
    type Error = Free;
    fn try_from(b: FreeState) -> Result<New, Free> {
        match b {
            FreeState::New => Ok(New),
            FreeState::Free => Err(Free)
        }
    }
}

pub trait IsBuiltState: Clone + PartialOrd + Into<BuiltState> + TryFrom<BuiltState> + From<New> {}
impl<T> IsBuiltState for T where T:
    Clone + PartialOrd + Into<BuiltState> + TryFrom<BuiltState> + From<New> {}

/// A definition with a given built state
pub struct BuiltDef<K, S = BuiltState> {
    def: DefId<K>,
    state: S
}

/// A free definition
pub type FreeDef<K> = BuiltDef<K, FreeState>;

/// A new definition
pub type NewDef<K> = BuiltDef<K, New>;

impl<K, S> BuiltDef<K, S> where K: Kind, S: IsBuiltState {
    /// Assert a definition is of the default of the given state
    #[inline(always)] pub fn assert_valid(def: DefId<K>) -> Self where S: Default {
        BuiltDef { def, state : Default::default() }
    }
    /// Assert a definition is of the given state
    #[inline(always)] pub fn assert_is_state(def: DefId<K>, state: S) -> Self {
        BuiltDef { def, state }
    }
    /// Assert the inner definition is valid for another kind
    #[inline(always)] pub fn assert_is_valid<L>(&self) -> BuiltDef<L, S> where L: Default + Kind {
        BuiltDef { def: DefId::assert_valid(self.def.ix()), state: self.state.clone() }
    }
    /// Assert the inner definition is another kind
    #[inline(always)] pub fn assert_is_kind<L>(&self, kind: L) -> BuiltDef<L, S> where L: Kind {
        BuiltDef { def: DefId::assert_is_kind(self.def.ix(), kind), state: self.state.clone() }
    }
    /// Get the index of the underlying definition
    #[inline(always)] pub fn ix(&self) -> NodeIndex<ValueIx> { self.def.ix() }
    /// Get the underlying definition
    #[inline(always)] pub fn def(&self) -> DefId<K> { self.def.clone() }
    /// Get the state
    #[inline(always)] pub fn state(&self) -> S { self.state.clone() }
    /// Weaken both the value handle and build state as necessary
    #[inline] pub fn weaken<L, M>(self) -> BuiltDef<L, M> where
        K: Into<L>,
        L: Kind,
        S: Into<M>,
        M: IsBuiltState {
        BuiltDef{ def : self.def.weaken(), state : self.state.into() }
    }
    /// Try to strengthen the state handle at the type level
    #[inline] pub fn strengthen<L>(self) -> Result<BuiltDef<K, L>, <S as TryInto<L>>::Error> where
        S: TryInto<L> {
        Ok(BuiltDef{ def : self.def, state : self.state.try_into()? })
    }
}

impl<K, S> From<DefId<K>> for BuiltDef<K, S> where K: Kind, S: IsBuiltState, Used: Into<S> {
    fn from(def: DefId<K>) -> BuiltDef<K, S> { BuiltDef{ def, state: Used.into() } }
}
impl<K, S> From<BuiltDef<K, S>> for DefId<K> where K: Kind, S: IsBuiltState {
    fn from(def: BuiltDef<K, S>) -> DefId<K> { def.def() }
}

/// An error which occurs when attempting to define a value from a `DefEdge`
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum DefError {
    /// A nonzero input
    NonZeroInput(NonZeroTupleIx)
}

impl<'a, M> ContextBuilder<'a, M> {
    /// Deep clone a value, returning a new value with no other values depending on it
    /// *and* no dependency on the original. Does not carry over metadata.
    pub fn deep_clone_value<K: Kind>(&mut self, val: DefId<K>) -> NewDef<K> {
        let edges_in: Vec<(NodeIndex<ValueIx>, EdgeKind)> = self.ctx.value_graph()
            .edges_directed(val.ix(), Direction::Incoming)
            .map(|edge| (edge.source(), *edge.weight()))
            .collect();
        let mut edges_in = edges_in.into_iter();
        let result: NodeIndex<ValueIx> = if let Some(first) = edges_in.next() {
            let (_, result) = self.ctx.value_dag.add_child(
                first.0, first.1,
                self.ctx.value_dag[val.ix()].metadata_stripped()
            );
            self.ctx.value_dag.add_edges(
                edges_in.map(|(source, kind)| (source, result, kind))
            ).expect("Cannot have cycles in clone operation");
            result
        } else {
            self.ctx.value_dag.add_node(self.ctx.value_dag[val.ix()].metadata_stripped())
        };
        NewDef::assert_valid(DefId::assert_is_kind(result, val.take_kind()))
    }
    /// Clone a value, reutrning a new value with no other values depening on it, with
    /// *only* a dependency on the original. Does not carry over metadata.
    pub fn clone_value<K: Kind>(&mut self, val: DefId<K>) -> NewDef<K> {
        let (_, result) = self.ctx.value_dag
            .add_child(val.ix(), EdgeKind::identity(), Definition::Expr(None));
        NewDef::assert_valid(DefId::assert_is_kind(result, val.take_kind()))
    }
    /// Clone a value if and only if other values depend on it. Return an identical value
    /// with nothing depending on it
    pub fn to_free_value<K: Kind>(&mut self, val: DefId<K>) -> FreeDef<K> {
        if self.ctx.has_deps(val.erase()) {
            self.clone_value(val)
                // Don't know why this needs an annotation...
                .weaken::<K, FreeState>()
        } else {
            FreeDef::assert_is_state(val, FreeState::Free)
        }
    }
    /// Strengthen a value to a given built state
    #[inline]
    pub fn strengthen<K, S>(&mut self, val: DefId<K>, state: S) -> BuiltDef<K, S>
        where K: Kind, S: IsBuiltState {
        use BuiltState::*;
        let target = state.clone();
        let state: BuiltState = state.into();
        BuiltDef::assert_is_state(
            match state {
                Used => val,
                Free => self.to_free_value(val).def(),
                New => self.clone_value(val).def()
            }, target)
    }
    /// Strengthen a built value to a given built state, if necessary
    #[inline]
    pub fn strengthen_built<K, I, O>(&mut self, def: BuiltDef<K, I>, state: O) -> BuiltDef<K, O>
        where
        K: Kind,
        I: IsBuiltState,
        O: IsBuiltState {
        let ds: BuiltState = def.state().into();
        let ts: BuiltState = state.clone().into();
        if ds >= ts {
            BuiltDef::assert_is_state(
                def.def(),
                ds.try_into()
                    // This branch should never happen, but just in case...
                    .unwrap_or(state)
            )
        } else {
            self.strengthen(def.def(), state)
         }
    }
    /// Build a definition edge with a target BuiltState. The result *may* need to be strengthened.
    #[inline]
    pub fn build_def_edge<E, K, SK, S>(&mut self, def: DefEdge<E, K, SK>, target: S)
        -> Result<BuiltDef<K, S>, DefError> where
        K: Kind, SK: Kind, S: IsBuiltState,
        E: Into<ValueEdge> + Clone {
        use ValueEdge::*;
        // Extract the source node and the value edge out
        let edge = def.edge().into();
        // Filter out nonzero inputs
        match edge {
            Expr{input} | Proj{input,..} | Inj{input,..} | Const{input,..} => {
                match NonZeroTupleIx::new(input) {
                    Some(input) => { return Err(DefError::NonZeroInput(input)) },
                    None => {}
                }
            }
        }
        // Match on the value edge
        match edge {
            Expr{..} => Ok(self.strengthen(
                DefId::assert_is_kind(def.ix(), def.kind()),
                target
            )),
            Proj{proj,..} => {
                let proj = self.build_proj(def.source().weaken(), proj, target.clone().into());
                let result = self.strengthen_built(proj, target);
                Ok(result.assert_is_kind(def.kind()))
            }
            _ => unimplemented!()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::context::value::kind::Value;
    use super::*;
    #[test]
    fn build_def_edge_nil_is_nil() {
        let mut ctx: Context<()> = Context::empty_with_capacity(0, 0, 0);
        let mut builder = ContextBuilder::new(&mut ctx);
        let def_edge = DefEdge::assert_is_kind(builder.get_nil(), ValueEdge::identity(), Value);
        let built = builder
            .build_def_edge(def_edge, Used)
            .expect("Identity is always valid");

        // Building an identity edge with no usage constraint should just give back the node
        assert_eq!(built.ix(), builder.get_nil().ix());

        // We do New before Free to assert when we call Free nil has one dependency
        // (the cloned value) and hence cannot be returned by a call for Free
        let built_cloned = builder
            .build_def_edge(def_edge, New)
            .expect("Identity is always valid");
        assert_ne!(built_cloned.ix(), builder.get_nil().ix());

        let built_free = builder
            .build_def_edge(def_edge, Free)
            .expect("Identity is always valid");
        assert_ne!(built_free.ix(), builder.get_nil().ix());
        //TODO: think about whether this should be left undefined
        assert_ne!(built_cloned.ix(), built_free.ix());
    }
    #[test]
    fn cloned_nil_is_not_get_nil() {
        let mut ctx: Context<()> = Context::empty_with_capacity(0, 0, 0);
        let mut builder = ContextBuilder::new(&mut ctx);
        let cloned_nil = builder.clone_value(builder.get_nil());
        assert_ne!(cloned_nil.ix(), builder.get_nil().ix());
        assert_eq!(
            builder.infer_type(cloned_nil.def().weaken())
                .expect("Cloned nil is a well-typed value"),
            builder.infer_type(builder.get_nil().weaken()).expect("Nil is a well-typed value")
        )
        //NOTE: it is not guaranteed that is_nil will return true for cloned_nil,
        //but not forbidden either. Hence, the behaviour is not tested.
    }
    #[test]
    fn deep_cloned_nil_is_true_nil() {
        let mut ctx: Context<()> = Context::empty_with_capacity(0, 0, 0);
        let mut builder = ContextBuilder::new(&mut ctx);
        let cloned_nil = builder.deep_clone_value(builder.get_nil());
        assert_ne!(cloned_nil.ix(), builder.get_nil().ix());
        assert!(builder.ctx().is_nil(cloned_nil.def().weaken()));

        //NOTE: it is not guaranteed that the type of cloned_nil is judgementally equal
        //to the type of get_nil(), but not forbidden either. Hence, the behaviour is not tested.
    }
    #[test]
    fn built_state_order() {
        use super::BuiltState::*;
        assert!(Used < Free);
        assert!(Free < New);
        assert!(Used < New);
    }
}
