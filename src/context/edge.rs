use std::convert::TryFrom;

use crate::TupleIx;

/// Isotope edge kinds
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum EdgeKind {
    // Metadata/typing edges

    /// An edge indicating the value pointed to is of the type pointed from.
    /// Respects scoping.
    Type,
    /// An edge indicating a value is in a subscope of another's.
    /// Respects scoping.
    Scope,

    // Value edges

    /// An edge indicating an operand of an S-expression or a member of a quote.
    /// Respects scoping. Does not project.
    Expr{ input : TupleIx },
    /// An edge indicating an operand of an S-expression or a member of a quote.
    /// Respects scoping.
    Proj{ input : TupleIx, proj : TupleIx },
    /// An edge indicating an operand of an S-expression or a member of a quote.
    /// Respects scoping. Injects (into a sum type).
    Inj{ input : TupleIx, inj : TupleIx },
    /// Inject this value into a constant function.
    /// 0 => polymorphic injection. May cause type inference to fail.
    Const{ input : TupleIx, times : TupleIx },

    // Function edges

    /// An edge indicating a function's result value. Does *not* respect scoping,
    Result{ output : TupleIx },
    /// An edge indicating a function parameter. Does *not* respect scoping.
    Parameter{ input : TupleIx },
}

impl EdgeKind {
    #[inline(always)] pub fn expr(input : TupleIx) -> Self { Self::Expr{ input } }
    #[inline(always)] pub fn identity() -> Self { Self::Expr{ input : 0 } }
    #[inline(always)] pub fn proj(input : TupleIx, proj : TupleIx) -> Self {
        Self::Proj { input, proj } }
    #[inline(always)] pub fn inj(input : TupleIx, inj : TupleIx) -> Self {
        Self::Inj { input, inj } }
    #[inline(always)] pub fn const_proj(input : TupleIx, times : TupleIx) -> Self {
        Self::Const { input, times } }
    #[inline(always)] pub fn const_poly(input : TupleIx) -> Self {
        Self::Const { input, times : 0 } }
    /// Whether this edge is a value input
    #[inline] pub fn is_value(&self) -> bool {
        use EdgeKind::*;
        match self { Expr{..} | Proj{..} | Inj{..} | Const{..} => true, _ => false }
    }
    /// Whether this edge is a typing judgement
    #[inline] pub fn is_type(&self) -> bool {
        use EdgeKind::*;
        match self { Type{..} => true, _ => false }
    }
    /// Whether this edge is an identity edge
    #[inline] pub fn is_identity(&self) -> bool {
        use EdgeKind::*;
        match self { Expr{input} => *input == 0, _ => false }
    }
}

impl From<TypeEdge> for EdgeKind {
    fn from(t: TypeEdge) -> Self { match t { TypeEdge::Type => Self::Type  } }
}
impl From<MetaEdge> for EdgeKind {
    fn from(m: MetaEdge) -> Self { match m { MetaEdge::Scope => Self::Scope  } }
}
impl From<ValueEdge> for EdgeKind {
    fn from(v: ValueEdge) -> Self {
        match v {
            ValueEdge::Expr{input} => Self::Expr{input},
            ValueEdge::Proj{input, proj} => Self::Proj{input, proj},
            ValueEdge::Inj{input, inj} => Self::Inj{input, inj},
            ValueEdge::Const{input, times} => Self::Const{input, times}
        }
    }
}
impl From<FunctionEdge> for EdgeKind {
    fn from(f: FunctionEdge) -> Self {
        match f {
            FunctionEdge::Result{output} => Self::Result{output},
            FunctionEdge::Parameter{input} => Self::Parameter{input}
        }
    }
}

/// Isotope type edges
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TypeEdge {
    Type
}

impl TryFrom<EdgeKind> for TypeEdge {
    type Error = EdgeKind;
    fn try_from(e: EdgeKind) -> Result<Self, Self::Error> {
        match e { EdgeKind::Type => Ok(Self::Type), _ => Err(e) }
    }
}

/// Isotope metadata edges
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MetaEdge {
    Scope
}

impl TryFrom<EdgeKind> for MetaEdge {
    type Error = EdgeKind;
    fn try_from(e: EdgeKind) -> Result<Self, Self::Error> {
        match e { EdgeKind::Scope => Ok(Self::Scope), _ => Err(e) }
    }
}

/// Isotope value edges
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ValueEdge {
    /// An edge indicating an operand of an S-expression or a member of a quote.
    /// Respects scoping. Does not project.
    Expr{ input : TupleIx },
    /// An edge indicating an operand of an S-expression or a member of a quote.
    /// Respects scoping.
    Proj{ input : TupleIx, proj : TupleIx },
    /// An edge indicating an operand of an S-expression or a member of a quote.
    /// Respects scoping. Injects (into a sum type).
    Inj{ input : TupleIx, inj : TupleIx },
    /// Inject this value into a constant function.
    /// 0 => polymorphic injection. May cause type inference to fail.
    Const{ input : TupleIx, times : TupleIx }
}

impl ValueEdge {
    #[inline(always)] pub fn expr(input : TupleIx) -> Self { Self::Expr{ input } }
    #[inline(always)] pub fn identity() -> Self { Self::Expr{ input : 0 } }
    #[inline(always)] pub fn proj(input : TupleIx, proj : TupleIx) -> Self {
        Self::Proj { input, proj } }
    #[inline(always)] pub fn inj(input : TupleIx, inj : TupleIx) -> Self {
        Self::Inj { input, inj } }
    #[inline(always)] pub fn const_proj(input : TupleIx, times : TupleIx) -> Self {
        Self::Const { input, times } }
    #[inline(always)] pub fn const_poly(input : TupleIx) -> Self {
        Self::Const { input, times : 0 } }
        
    /// Whether this edge is an identity edge
    #[inline] pub fn is_identity(&self) -> bool {
        use ValueEdge::*;
        match self { Expr{input} => *input == 0, _ => false }
    }
}

impl TryFrom<EdgeKind> for ValueEdge {
    type Error = EdgeKind;
    fn try_from(e: EdgeKind) -> Result<Self, Self::Error> {
        match e {
            EdgeKind::Expr{input} => Ok(Self::Expr{input}),
            EdgeKind::Proj{input, proj} => Ok(Self::Proj{input, proj}),
            EdgeKind::Inj{input, inj} => Ok(Self::Inj{input, inj}),
            EdgeKind::Const{input, times} => Ok(Self::Const{input, times}),
            _ => Err(e)
        }
    }
}

/// Isotope function edge
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum FunctionEdge {
    /// An edge indicating a function's result value. Does *not* respect scoping,
    Result{ output : TupleIx },
    /// An edge indicating a function parameter. Does *not* respect scoping.
    Parameter{ input : TupleIx },
}

impl TryFrom<EdgeKind> for FunctionEdge {
    type Error = EdgeKind;
    fn try_from(e: EdgeKind) -> Result<Self, Self::Error> {
        match e {
            EdgeKind::Result{output} => Ok(Self::Result{output}),
            EdgeKind::Parameter{input} => Ok(Self::Parameter{input}),
            _ => Err(e)
        }
    }
}
