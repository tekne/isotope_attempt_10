use petgraph::graph::NodeIndex;
use std::convert::TryFrom;
use std::convert::TryInto;

use crate::ValueIx;
use super::{MetadataId, edge::ValueEdge};

pub mod kind;
use kind::{
    Kind, Value, Type, DefinitionKind
};

/// An isotope value definition
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Definition {

    // Primitive Types and Values:

    /**
    A polymorphic universe
    TODO: metadata, Finite, synthesizability
    */
    Universe(Option<MetadataId>),

    // Type Expressions:

    /**
    Take the product of the types passed in, where all subsequent types are functions
    of all previous types.
    No arguments gives the unit type. One argument is identity.
    */
    Product(Option<MetadataId>),
    /**
    Take the sum of the types passed in, where all subsequent types are functions of
    all previous types.
    No arguments gives the empty type. One argument is identity.
    */
    Sum(Option<MetadataId>),
    /**
    A curried function type, where all subsequent types are functions of all previous
    types. Use a numbered ``const'' edge to opt out.
    No arguments gives the unit type. One argument is identity.
    */
    Function(Option<MetadataId>),

    // Expressions:

    /**
    An S-expression. Defined as the result of currying the last parameter
    inserted with previous ones.
    The empty S-expression is the null tuple, or Nil.
    The S-expression with one value just takes on its value.
    */
    Expr(Option<MetadataId>),
    /**
    Quote a group of arguments together into a (potentially dependently-typed) tuple.
    No arguments => Nil.
    */
    Quote(Option<MetadataId>),
    /**
    Catenate a group of dependently typed tuples. No tuples => Nil. Catenation with Nil is identity
    */
    Cat(Option<MetadataId>),

    // Functions:

    /**
    A function definition.
    */
    Lambda(Option<MetadataId>),
    /**
    A symbol, which can be used as an assumption of a type if given a type by a Type edge.
    */
    Symbol(Option<MetadataId>)
}

impl Definition {
    #[inline]
    pub fn get_meta_id_mut(&mut self) -> &mut Option<MetadataId> {
        use Definition::*;
        match self {
            Universe(m) | Product(m) | Sum(m) | Function(m) |
            Expr(m) | Quote(m) | Cat(m) | Lambda(m) | Symbol(m) => m
        }
    }
    #[inline]
    pub fn get_meta_id(&self) -> Option<MetadataId> {
        use Definition::*;
        match self {
            Universe(m) | Product(m) | Sum(m) | Function(m) |
            Expr(m) | Quote(m) | Cat(m) | Lambda(m) | Symbol(m) => *m
        }
    }
    #[inline]
    pub fn is_quote(&self) -> bool {
        use Definition::*;
        match self { Quote(_) => true, _ => false }
    }
    /// Get the kind of this definition
    #[inline]
    pub fn get_definition_kind(&self) -> DefinitionKind {
        match self {
            Definition::Universe(_) => DefinitionKind::Universe,
            Definition::Product(_) => DefinitionKind::Product,
            Definition::Sum(_) => DefinitionKind::Sum,
            Definition::Function(_) => DefinitionKind::Function,
            Definition::Expr(_) => DefinitionKind::Expr,
            Definition::Quote(_) => DefinitionKind::Quote,
            Definition::Cat(_) => DefinitionKind::Cat,
            Definition::Lambda(_) => DefinitionKind::Lambda,
            Definition::Symbol(_) => DefinitionKind::Symbol,
        }
    }
    /// Assert a value is of the same kind as this definition. May result in errors if this
    /// is not the case!
    #[inline]
    pub fn assert_definition_kind(&self, val: NodeIndex<ValueIx>) -> DefId<DefinitionKind> {
        DefId::assert_is_kind(val, self.get_definition_kind())
    }
    /// Get any metadata this definition may hold
    #[inline]
    pub fn get_metadata(&self) -> Option<MetadataId> {
        *match self {
            Definition::Universe(meta) => meta,
            Definition::Product(meta) => meta,
            Definition::Sum(meta) => meta,
            Definition::Function(meta) => meta,
            Definition::Expr(meta) => meta,
            Definition::Quote(meta) => meta,
            Definition::Cat(meta) => meta,
            Definition::Lambda(meta) => meta,
            Definition::Symbol(meta) => meta,
        }
    }
    /// Get a mutable reference to which metadata is associated with this definition
    #[inline]
    pub fn get_metadata_mut(&mut self) -> &mut Option<MetadataId> {
        match self {
            Definition::Universe(meta) => meta,
            Definition::Product(meta) => meta,
            Definition::Sum(meta) => meta,
            Definition::Function(meta) => meta,
            Definition::Expr(meta) => meta,
            Definition::Quote(meta) => meta,
            Definition::Cat(meta) => meta,
            Definition::Lambda(meta) => meta,
            Definition::Symbol(meta) => meta,
        }
    }
    /// Get this definition with metadata stripped
    #[inline]
    pub fn metadata_stripped(&self) -> Self {
        let mut result = self.clone();
        *result.get_metadata_mut() = None;
        result
    }
}

/// An index which is asserted at compile-time to point to a valid isotope
/// definition of the kind given
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct DefId<K> {
    ix : NodeIndex<ValueIx>,
    kind : K
}

impl<K> Into<NodeIndex<ValueIx>> for DefId<K> {
    fn into(self) -> NodeIndex<ValueIx> { self.ix }
}

impl<K> DefId<K> where K: Kind {
    /// Assert a definition ID is the valid default of this kind
    #[inline] pub fn assert_valid<I>(ix: I) -> Self where
        K: Default, I: Into<NodeIndex<ValueIx>> {
        DefId{ ix: ix.into(), kind : K::default() } }
    /// Assert a definition ID is of a given kind
    #[inline] pub fn assert_is_kind<I>(ix: I, kind: K) -> Self where
        I: Into<NodeIndex<ValueIx>> {
        DefId{ ix: ix.into(), kind }}
    /// Get the index of this definition ID
    #[inline(always)] pub fn ix(&self) -> NodeIndex<ValueIx> { self.ix }
    /// Get the kind of this definition ID
    #[inline(always)] pub fn kind(&self) -> &K { &self.kind }
    /// Move out this definition's kind
    #[inline(always)] pub fn take_kind(self) -> K { self.kind }
    /// Weaken this definition ID to a superkind
    #[inline]
    pub fn weaken<S>(self) -> DefId<S> where K: Into<S> {
        DefId{ ix : self.ix, kind : self.kind.into() } }
    /// Forget some of the kind information of this definition ID
    #[inline]
    pub fn erase<S>(&self) -> DefId<S> where K: Into<S>, S: Default {
        DefId{ ix : self.ix, kind : S::default() }
    }
}

pub type ValId = DefId<Value>;
pub type TypeId = DefId<Type>;

impl<E, K, S> TryFrom<DefEdge<E, K, S>> for DefId<K> where
    K: Kind, S: Kind, E: TryInto<ValueEdge> {
    type Error = ();
    fn try_from(de: DefEdge<E, K, S>) -> Result<DefId<K>, ()> {
        let e : Result<ValueEdge, _> = de.edge_kind.try_into();
        match e {
            Ok(e) => if e.is_identity() {
                Ok(DefId::assert_is_kind(de.source.ix(), de.kind)) } else { Err(()) },
            Err(_) => Err(())
        }
    }
}

impl<E, K> From<DefId<K>> for DefEdge<E, K, K> where K: Into<Value>, ValueEdge: Into<E>, K: Clone {
    fn from(ve: DefId<K>) -> DefEdge<E, K, K> {
        let new_kind = ve.kind.clone();
        DefEdge { source: ve, kind: new_kind, edge_kind: ValueEdge::identity().into() }
    }
}

/// An edge-kind out of an index which is asserted at compile-time to point to
/// a valid isotope definition of the kind given
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct DefEdge<E, K, S = Value> {
    source : DefId<S>,
    edge_kind: E,
    kind : K
}

impl<E: Clone, K: Kind, S: Kind> DefEdge<E, K, S> where K: Into<Value>, S: Into<Value> {
    /// Assert a definition is the valid default of this kind
    #[inline(always)] pub fn assert_valid(source: DefId<S>, edge_kind: E)
    -> Self where K: Default { DefEdge{ source, edge_kind, kind : K::default() } }
    /// Assert a definition is of a given kind
    #[inline(always)] pub fn assert_is_kind(source: DefId<S>, edge_kind: E, kind: K)
    -> Self { DefEdge{ source, edge_kind, kind }}
    /// Get this definition's source index
    #[inline(always)] pub fn ix(&self) -> NodeIndex<ValueIx> { self.source.ix() }
    /// Get this definition's source
    #[inline(always)] pub fn source(&self) -> DefId<S> { self.source.clone() }
    /// Get the (result) kind of this definition
    #[inline(always)] pub fn kind(&self) -> K { self.kind.clone() }
    /// Get the edge kind of this definition
    #[inline(always)] pub fn edge(&self) -> E { self.edge_kind.clone() }
    /// Weaken this definition
    #[inline]
    pub fn weaken<L, M, N>(self) -> DefEdge<L, M, N> where E: Into<L>, K: Into<M>, S: Into<N> {
        DefEdge {
            source: self.source.weaken(),
            edge_kind: self.edge_kind.into(),
            kind: self.kind.into()
        }
    }
    /// Erase this definition's source kind
    #[inline]
    pub fn erase_src(self) -> DefEdge<E, K> {
        DefEdge{
            source: ValId::assert_valid(self.source.ix()),
            edge_kind: self.edge_kind, kind: self.kind
        }
    }
}

impl<E, K, S> DefEdge<E, K, S> where E: TryInto<ValueEdge>, K: Into<Value>, S: Into<Value> {
    pub fn is_identity(self) -> bool {
        let e : Result<ValueEdge, _> = self.edge_kind.try_into();
        match e {
            Ok(e) => e.is_identity(),
            Err(..) => false
        }
    }
}

pub type ValDef = DefEdge<ValueEdge, Value>;
