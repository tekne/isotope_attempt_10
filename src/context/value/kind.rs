use crate::context::{
    edge::ValueEdge,
    builder::{ContextBuilder, state::{BuiltDef, FreeDef, NewDef, New, FreeState, BuiltState}}
};
use super::{DefId, DefEdge};

/// A type interpreted as a value kind
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AsKind<K>(pub K);

/// A marker type indicating a valid isotope value
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Value;
impl Kind for Value {}

/// A helper macro to easily implement Kind
macro_rules! impl_kind {
    ($k:ty) => {
        impl From<$k> for Value { #[inline(always)] fn from(_: $k) -> Value { Value } }
        impl Kind for $k {}
    }
}

/// A trait implemented by all value kinds
pub trait Kind: Into<Value> + Clone {}

/// A valid isotope value asserted to be a type (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Type;
impl_kind!(Type);
/// A marker type indicating a valid isotope value asserted to be a quote (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Quote;
impl_kind!(Quote);
/// A marker type indicating a valid isotope value asserted to be a universe (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Universe;
impl_kind!(Universe);
/// A marker type indicating a valid isotope value asserted to be a product type (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Product;
impl_kind!(Product);
/// A marker type indicating a valid isotope value asserted to be a sum type (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Sum;
impl_kind!(Sum);
/// A marker type indicating a valid isotope value asserted to be a function type (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Function;
impl_kind!(Function);
/// A marker type indicating a valid isotope value asserted to be an expression (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Expr;
impl_kind!(Expr);
/// A marker type indicating a valid isotope value asserted to be a catenation (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Cat;
impl_kind!(Cat);
/// A marker type indicating a valid isotope value asserted to be a lambda (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Lambda;
impl_kind!(Lambda);
/// A marker type indicating a valid isotope value asserted to be a symbol (at time of construction).
/// May be invalidated by later usage of the value, so tread with caution!
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Symbol;
impl_kind!(Symbol);

/// An isotope value definition kind
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum DefinitionKind {
    Universe,
    Product,
    Sum,
    Function,
    Expr,
    Quote,
    Cat,
    Lambda,
    Symbol
}
impl_kind!(DefinitionKind);

/// The trait implemented by objects which can be converted into a value of kind K
/// with a context builder
pub trait IntoValue<K>: Sized where K: Kind {
    /// Convert this value into an index pointing to a valid definition node
    fn into_val<M>(self, builder: &mut ContextBuilder<M>) -> BuiltDef<K>;
    /// Convert this value into an index pointing to a valid definition node and
    /// not depended on by any other nodes
    fn into_free_val<M>(self, builder: &mut ContextBuilder<M>) -> FreeDef<K> {
        let val = self.into_val(builder);
        builder.strengthen_built(val, BuiltState::Free).strengthen().expect("Failed to strengthen")
    }
    /// Convert this value into an index pointing to a new valid definition node
    /// not depended on by any other nodes
    fn into_new_val<M>(self, builder: &mut ContextBuilder<M>) -> NewDef<K> {
        let val = self.into_val(builder);
        builder.strengthen_built(val, BuiltState::New).strengthen().expect("Failed to strengthen")
    }
}

impl<K: Kind> IntoValue<K> for DefId<K> {
    fn into_val<M>(self, _builder: &mut ContextBuilder<M>) -> BuiltDef<K> { self.into() }
}

impl<E: Clone + Into<ValueEdge>, K: Kind, S: Kind> IntoValue<K> for DefEdge<E, K, S> {
    fn into_val<M>(self, builder: &mut ContextBuilder<M>) -> BuiltDef<K> {
        builder.build_def_edge(self, BuiltState::Used).expect("This edge was asserted to be valid")
    }
    fn into_free_val<M>(self, builder: &mut ContextBuilder<M>) -> FreeDef<K> {
        builder.build_def_edge(self, FreeState::Free).expect("This edge was asserted to be valid")
    }
    fn into_new_val<M>(self, builder: &mut ContextBuilder<M>) -> NewDef<K> {
        builder.build_def_edge(self, New).expect("This edge was asserted to be valid")
    }
}

/// The trait implemented by objects which can be converted into a value of kind K
/// with a context builder where this conversion may fail
pub trait TryIntoValue<K> {
    type Error;
    /// Convert this value into an index pointing to a valid definition node
    fn try_into_val<M>(self, builder: &mut ContextBuilder<M>) -> Result<BuiltDef<K>, Self::Error>;
    /// Convert this value into an index pointing to a valid definition node and
    /// not depended on by any other nodes
    fn try_into_free_val<M>(self, builder: &mut ContextBuilder<M>)
    -> Result<FreeDef<K>, Self::Error>;
    /// Convert this value into an index pointing to a new valid definition node
    fn try_into_new_val<M>(self, builder: &mut ContextBuilder<M>)
    -> Result<NewDef<K>, Self::Error>;
}

impl<T, K: Kind> TryIntoValue<K> for T where T: IntoValue<K> {
    type Error = !;
    fn try_into_val<M>(self, builder: &mut ContextBuilder<M>) -> Result<BuiltDef<K>, !> {
        Ok(self.into_val(builder))
    }
    fn try_into_free_val<M>(self, builder: &mut ContextBuilder<M>) -> Result<FreeDef<K>, !> {
        Ok(self.into_free_val(builder))
    }
    fn try_into_new_val<M>(self, builder: &mut ContextBuilder<M>) -> Result<NewDef<K>, !> {
        Ok(self.into_new_val(builder))
    }
}
