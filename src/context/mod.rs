use petgraph::graph::{DiGraph as Graph};
use petgraph::graph::NodeIndex;
use petgraph::visit::EdgeRef;
use petgraph::Direction;
use daggy::Dag;
use internship::IStr;

use crate::ValueIx;

pub mod edge;
use edge::EdgeKind;
pub mod builder;
pub mod value;
use value::{
    DefId, Definition, ValId, TypeId,
    kind::{Universe, Product, Expr, Quote, DefinitionKind}
};

//TODO: finite universe!
/// The trait implemented by metadata optionally associated with a value
pub trait Metadata : Sized {
    /// Get metadata, if any, associated with nil
    #[inline] fn get_nil_meta() -> Option<Self> { None }
    /// Get metadata, if any, associated with the unit type
    #[inline] fn get_unit_meta() -> Option<Self> { None }
    /// Get metadata, if any, associated with the empty type
    #[inline] fn get_empty_meta() -> Option<Self> { None }
    /// Get metadata, if any, associated with the standard universe
    #[inline] fn get_universe_meta() -> Option<Self> { None }
    /// Get the name, if any, of the given object
    #[inline] fn get_name(&self) -> Option<&str> { None }
    /// Get the interned name, if any, of the given object
    #[inline] fn get_interned_name(&self) -> Option<IStr> { self.get_name().map(|n| IStr::new(n)) }
}

impl Metadata for () {}

/// An isotope context, containing all scopes and values as well as cached type information.
#[derive(Debug, Clone)]
pub struct Context<M = ()> {
    /// The graph of value definitions in this context
    value_dag : Dag<Definition, EdgeKind, ValueIx>,
    /// The metadata in this context
    value_metadata : Vec<M>
}

impl<M> Context<M> {
    /// Create a new empty isotope context with a capacity for a given amount
    /// of values and dependencies
    pub fn empty_with_capacity(values: usize, dependencies: usize, metadata: usize) -> Context<M> {
        let mut value_dag = Dag::with_capacity(values + 1, dependencies);
        // Add the root
        value_dag.add_node(Definition::Quote(None));
        let value_metadata = Vec::with_capacity(metadata + 1);
        Context { value_dag, value_metadata }
    }
    /// Set the metadata of a given value
    pub fn set_meta<N: Into<M>>(&mut self, val: ValId, new: N) -> Option<M> {
        let val = self.value_dag[val.ix()].get_meta_id_mut();
        match val {
            Some(id) => {
                let mut res : M = new.into();
                std::mem::swap(&mut self.value_metadata[id.idx()], &mut res);
                Some(res)
            },
            None => {
                *val = Some(MetadataId::new(self.value_metadata.len()));
                self.value_metadata.push(new.into());
                None
            }
        }
    }
    /// Get the metadata of a given value
    #[inline]
    pub fn get_meta(&self, val: ValId) -> Option<&M> {
        self.value_dag[val.ix()].get_meta_id().map(|id| &self.value_metadata[id.idx()])
    }
    /// Transform an index into a valid value, if possible
    #[inline]
    pub fn value_at(&self, ix: NodeIndex<ValueIx>) -> Option<ValId> {
        if self.value_dag.raw_nodes().len() > ix.index() {
            Some(ValId::assert_valid(ix)) } else { None }
    }
    /// Transform a value index into a quote index, if possible
    #[inline]
    pub fn to_quote(&self, val: ValId) -> Option<DefId<Quote>> {
        if self.value_dag[val.ix()].is_quote() {
            Some(DefId::assert_valid(val.ix())) } else { None }
    }
    /// "Kind" a value, if possible
    #[inline]
    pub fn to_value_kind(&self, val: ValId) -> DefId<DefinitionKind> {
        self.value_dag[val.ix()].assert_definition_kind(val.ix())
    }
    /// Count the dependencies of a value
    #[inline]
    pub fn count_deps(&self, val: ValId) -> usize {
        self.value_graph()
            .edges_directed(val.ix(), Direction::Incoming)
            .count()
    }
    /// Get the members of a quote
    #[inline]
    pub fn quote_members(&self, qt: DefId<Quote>) -> impl Iterator<Item=ValId> + '_ {
        self.value_graph()
            .edges_directed(qt.ix(), Direction::Incoming)
            .filter(|edge| edge.weight().is_value() )
            .map(|edge| ValId::assert_valid(edge.source()))
    }
    /// Get the members of a product
    #[inline]
    pub fn product_members(&self, qt: DefId<Product>) -> impl Iterator<Item=TypeId> + '_ {
        self.value_graph()
            .edges_directed(qt.ix(), Direction::Incoming)
            .filter(|edge| edge.weight().is_value() )
            .map(|edge| TypeId::assert_valid(edge.source()))
    }
    /// Get the root value of this context
    #[inline(always)]
    pub fn get_root(&self) -> ValId { ValId::assert_valid(NodeIndex::new(0)) }
    /// Get a new nil value in this context
    #[inline]
    pub fn get_new_nil(&mut self) -> DefId<Expr> {
        DefId::assert_valid(self.value_dag.add_node(Definition::Expr(None))) }
    /// Get a new nil tuple in this context
    #[inline]
    pub fn get_new_nil_tup(&mut self) -> DefId<Quote> {
        DefId::assert_valid(self.value_dag.add_node(Definition::Quote(None))) }
    /// Check whether a value is a (valid) nil
    pub fn is_nil(&self, val : ValId) -> bool {
        match self.value_dag[val.ix()] {
            Definition::Expr(_) | Definition::Quote(_) => {
                !self.value_graph()
                    .edges_directed(val.ix(), Direction::Incoming)
                    .any(|edge| edge.weight().is_value())
            },
            _ => false
        }
    }
    /// Check whether a type is a (valid) unit
    pub fn is_unit(&self, val : TypeId) -> bool {
        match self.value_dag[val.ix()] {
            Definition::Product(_) => {
                !self.value_graph()
                    .edges_directed(val.ix(), Direction::Incoming)
                    .any(|edge| edge.weight().is_value())
            },
            _ => false
        }
    }
    /// Get a unit type in this context
    #[inline]
    pub fn get_new_unit(&mut self) -> TypeId {
        TypeId::assert_valid(self.value_dag.add_node(Definition::Product(None))) }
    /// Get an empty type in this context
    #[inline]
    pub fn get_new_empty(&mut self) -> TypeId {
        TypeId::assert_valid(self.value_dag.add_node(Definition::Sum(None))) }
    /// Get a universe in this context
    #[inline]
    pub fn get_new_universe(&mut self) -> TypeId {
        TypeId::assert_valid(self.value_dag.add_node(Definition::Universe(None))) }
    /// Try to get the type of a value, if it has been computed *or* if it's trivial
    #[inline]
    pub fn get_type(&self, val : ValId) -> Result<InferredType, TypeError> {
        use TypeError::*;
        use InferredType::*;
        let mut res = Err(NotEnoughInfo);
        let ty_edges = self.value_graph()
            .edges_directed(val.ix(), Direction::Incoming)
            .filter(|edge| edge.weight().is_type());
        for edge in ty_edges {
            match res {
                Err(NotEnoughInfo) => { res = Ok(HasType(TypeId::assert_valid(edge.source()))) },
                //TODO: check for equality, maybe...
                Ok(_) => unimplemented!(),
                Err(_) => unreachable!()
            }
        }
        //TODO: universes
        res
    }
    /// Check if a value has dependencies
    pub fn has_deps(&self, val: ValId) -> bool {
        self.value_graph().edges_directed(val.ix(), Direction::Outgoing).any(|_edge| true)
    }
    /// Get an immutable reference to the value DAG
    #[inline(always)]
    pub fn value_dag(&self) -> &Dag<Definition, EdgeKind, ValueIx> { &self.value_dag }
    /// Get an immutable reference to the value graph
    #[inline(always)]
    pub fn value_graph(&self) -> &Graph<Definition, EdgeKind, ValueIx> { &self.value_dag.graph() }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum InferredType {
    /// Has a given type
    HasType(TypeId),
    /// Is a root universe. Contains the ID for convenience.
    /// We try to avoid inferring root universe types to keep the graph finite.
    Universe(DefId<Universe>)
}

/// A type error
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TypeError {
    /// Not enough information to determine the type
    NotEnoughInfo,
    /// Cyclic type dependency
    CyclicTypeDep
}

/// An entry into the isotope metadata store
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct MetadataId(std::num::NonZeroUsize);

impl MetadataId {
    #[inline]
    fn new(idx: usize) -> MetadataId { MetadataId(std::num::NonZeroUsize::new(idx + 1).unwrap()) }
    #[inline] pub fn idx(&self) -> usize { self.0.get() - 1 }
}
