#![feature(never_type)]
use nonzero_ext::NonZeroAble;

/// The integer type used to index values
pub type ValueIx = usize;
/// The integer type used to index tuples
pub type TupleIx = u32;
/// A nonzero tuple index
pub type NonZeroTupleIx = <TupleIx as NonZeroAble>::NonZero;

pub mod context;
pub mod parser;
